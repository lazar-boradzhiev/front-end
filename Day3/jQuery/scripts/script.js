
$(document).ready(() => {

	var imgName = 1;

	$("#hide-btn").click(() => {
		$("#img").hide();
	});

	$("#show-btn").click(() => {
		$("#img").show();
	});

	$("#round-corners-btn").click(() => {
		$("#img").removeClass("straight-corners");
		$("#img").addClass("round-corners");
	});

	$("#straight-corners-btn").click(() => {
		$("#img").removeClass("round-corners");
		$("#img").addClass("straight-corners");
	});

	$("#add-red-border-btn").click(() => {
		$("#img").removeClass("no-red-border");
		$("#img").addClass("red-border")
	});

	$("#remove-red-border-btn").click(() => {
		$("#img").removeClass("red-border")
		$("#img").addClass("no-red-border");
	});

	$("#move-up-btn").click(() => {
		$("#img").animate({top: "-=10"}, 100)
	});

	$("#move-down-btn").click(() => {
		$("#img").animate({top: "+=10"}, 100)
	});

	$("#next-img-btn").click(() => {
		if (imgName === 3) {
			imgName = 1;
		}
		else{
			imgName++;
		}
		$("#img").attr("src", "img/" + imgName + ".jpg");
	});

	$("#previous-img-btn").click(() => {
		if (imgName === 1) {
			imgName = 3;
		}
		else{
			imgName--;
		}
		$("#img").attr("src", "img/" + imgName + ".jpg");
	});
})