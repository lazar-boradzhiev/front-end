$(document).ready(()=>{
		$("#fetch-btn").click(() => {
			$.ajax({
			url: "https://jsonplaceholder.typicode.com/users",
			data: "PlainObject",
			dataType: "json"
		})
		.done( (json) => {
			
			var table = document.getElementById("users-table");
			var row;

			for (var i = 0; i < 10; i++) {
				row = table.insertRow(i + 1)
				row.insertCell(0).innerHTML = json[i].name;
				row.insertCell(1).innerHTML = json[i].username;
				row.insertCell(2).innerHTML = json[i].email;
			}
		})
	// fetch('https://jsonplaceholder.typicode.com/users')
	// .then(response => response.json())
	// .then(json => {

	// 	var table = document.getElementById("users-table");
	// 	var row;

	// 	for (var i = 0; i < 10; i++) {
	// 		row = table.insertRow(i + 1)
	// 		row.insertCell(0).innerHTML = json[i].name;
	// 		row.insertCell(1).innerHTML = json[i].username;
	// 		row.insertCell(2).innerHTML = json[i].email;
	// 	}
	})
})