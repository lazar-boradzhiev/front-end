$(document).ready(() => {

      createChartUsingGoogleCharts();

      createChartUsingChartJs();

})

function createChartUsingChartJs(){
  new Chart(document.getElementById("bar-chart"), {
      type: 'bar',
      data: {
        labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
        datasets: [
          {
            label: "Population (millions)",
            backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
            data: [2478,5267,734,784,433]
          }
        ]
      },
      options: {
        legend: { display: false },
        title: {
          display: true,
          text: 'Predicted world population (millions) in 2050'
        }
      }
  });


}

function createChartUsingGoogleCharts(){
  google.charts.load('current', {'packages':['corechart']});

      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Continent');
        data.addColumn('number', 'Population (millions)');
        data.addRows([
          ['Africa', 2478],
          ['Asia', 5267],
          ['Europe', 734],
          ['Latin America', 784],
          ['North America', 433]
        ]);

        var options = {'title':'Predicted world population (millions) in 2050',
                       'width':1000,
                       'height':900};

        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
}