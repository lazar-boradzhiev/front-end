$(document).ready( () => {

    var form = $("#users-form");

    $.ajax({
        url: "https://jsonplaceholder.typicode.com/users/1",
        method: "GET",
    }).done(function (data, textStatus, jqXHR) {
         
        generateForm(form, data);
        
    });

    validateForm(form);

});

function generateForm(form, data){

    //var fieldset = document.createElement("fieldset");
    //form.append(fieldset);

    for(var item in data) {

            var label = document.createElement("label");
            label.innerHTML = item + ": ";

            var input = document.createElement("input");
            if (item == "email") {
                input.type = "email"
            }
            else if (item == "website") {
                input.type = "url"
            }
            else {
                input.type = "text";
            }
            
            input.name = item;

            form.append(label);
            form.append(input);
            form.append("<br/>")
            form.append("<br/>")
        }

        var input = document.createElement("input");
        input.type = "submit";
        input.value = "Submit";

        form.append(input);
}

function validateForm(form){

    form.validate({

        rules: {
            email: {
                required: true,
                email: true
            }
        },

        submitHandler: (_form) => { 

            $.ajax({
                url: _form.action,
                type: _form.method,
                data: $(_form).serialize(),
                success: ((data, textStatus, jqXHR) => {
                    $( () => {
                        new PNotify({
                            title: jqXHR.status,
                            text: textStatus,
                            type: 'success'
                        });
                    });
                }),
                error: ((data, textStatus, jqXHR) => {
                    $( () => {
                        new PNotify({
                            title: jqXHR.status,
                            text: textStatus,
                            type: 'error'
                        });
                    });
                }),          
            });

        }
    });
}